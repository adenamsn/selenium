const {Builder, By, Key, until} = require('selenium-webdriver');
const assert = require('assert');
const fs = require('fs');

const writeScreenshot = (data, name) => fs.writeFileSync(name, data, 'base64');
test('Should produce 48 as an output after multiplying 6 and 8', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
  await (await driver.findElement({ name: 'six' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'eight' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  assert.strictEqual(result, '48');

  driver.takeScreenshot().then((data) => writeScreenshot(data, 'output1.png'));
  await driver.quit();
});

test('Should produce 36 as an output after multiplying 4 and 9', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');

  await (await driver.findElement({ name: 'four' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'nine' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  assert.strictEqual(result, '36');

  driver.takeScreenshot().then((data) => writeScreenshot(data, 'output2.png'));
  await driver.quit();
});



test('Should produce 60 as an output after 3 multiplying 4 and multiplying 5', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
  await (await driver.findElement({ name: 'three' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'four' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'five' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  assert.strictEqual(result, '60');

  driver.takeScreenshot().then((data) => writeScreenshot(data, 'output3.png'));
  await driver.quit();
});